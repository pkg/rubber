Source: rubber
Section: tex
Priority: optional
Maintainer: Hilmar Preusse <hille42@web.de>
Uploaders: Debian Python Team <team+python@tracker.debian.org>
Build-Depends:
 debhelper-compat (= 12),
 dh-python,
 python3,
 texinfo,
 texlive-latex-base,
Standards-Version: 4.5.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/python-team/packages/rubber
Vcs-Git: https://salsa.debian.org/python-team/packages/rubber.git
Homepage: https://launchpad.net/rubber

Package: rubber
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}, texlive-latex-base
Suggests:
 asymptote,
 biber,
 imagemagick,
 python3-prompt-toolkit,
 python3-pygments,
 r-cran-knitr,
 texlive-bibtex-extra,
 texlive-binaries,
 texlive-extra-utils,
 texlive-latex-extra,
 texlive-latex-recommended,
 texlive-metapost,
 texlive-pictures,
 transfig
Description: automated system for building LaTeX documents
 This is a building system for LaTeX documents. It is based on a routine that
 runs just as many compilations as necessary. The module system provides a
 great flexibility that virtually allows support for any package with no user
 intervention, as well as pre- and post-processing of the document. The
 standard modules currently provide support for bibtex, dvips, dvipdfm,
 pdftex, makeindex. A good number of standard packages are supported,
 including graphics/graphicx with automatic conversion between various
 graphics formats and Metapost compilation.
 .
 Some optional parts require the installation of suggested Debian packages:
 imagemagick (graphics conversion),
 r-cran-knitr,
 texlive-bibtex-extra (biblatex),
 texlive-binaries (aleph),
 texlive-latex-extra (cweb, minitoc, moreverb, multibib, nomencl, ntheorem),
 texlive-latex-recommended (beamer, index, listings),
 texlive-pictures (gnuplottex),
 transfig (conversion from XFig format).
 .
 pythontex from texlive-extra-utils requires
 python3-prompt-toolkit and python3-pygments.
